#include <map>
#include <vector>
#include <iostream>
#include <algorithm>
#include <memory>
#include <type_traits>

template<typename T>
struct Node
{
    Node(T val) : value(val) {}; 
    T value;
};

template<typename T>
class Graph
{
    public:
        typedef std::shared_ptr<Node<T>> Vertex;
        typedef std::weak_ptr<Node<T>> Connection;

        void addVertex(const Vertex& vertex)
        {
            _graph.insert({std::move(vertex), {}});
        }
        void removeVertex(Vertex& vertex)
        {
            _graph.erase(vertex);
            vertex.reset();
        }
        void addConnection(const Connection& from, const Connection& to)
        {
            _graph.at(from.lock()).push_back(to);
            _graph.at(to.lock()).push_back(from);
        }
        void print()
        {
            for(const auto& vertex : _graph)
            {
                std::cout << vertex.first.get()->value << " - > ";
                for(const auto& connection: vertex.second)
                {
                    if(auto locked = connection.lock(); locked != nullptr)
                    {
                        std::cout << locked.get()->value << " ";
                    }
                }
                std::cout << std::endl;
            }
            std::cout << std::endl;
        }
    private:
        std::map<Vertex, std::vector<Connection>> _graph;
};

int main()
{
    Graph<int> graph;

    auto val_1 = std::make_shared<Node<int>>(1);
    graph.addVertex(val_1);

    auto val_3 = std::make_shared<Node<int>>(3);
    graph.addVertex(val_3);

    auto val_1_second = std::make_shared<Node<int>>(1);
    graph.addVertex(val_1_second);

    graph.addConnection(val_1, val_3);
    graph.addConnection(val_3, val_1_second);
    graph.addConnection(val_1, val_1_second);

    // Expected Output:
    /*
    1 - > 3 1
    3 - > 1 1
    1 - > 3 1
    */

    graph.print();

    graph.removeVertex(val_3);
    // Expected Output:
    /*
    1 - > 1
    1 - > 1
    */
    graph.print();

    auto val_5 = std::make_shared<Node<int>>(5);
    graph.addVertex(val_5);
    // Expected Output:
    /*
    1 - > 1
    1 - > 1
    5 - >
    */
    graph.print();

    graph.removeVertex(val_1);
    // Expected Output:
    /*
    1 - >
    5 - >
    */
    graph.print();
}